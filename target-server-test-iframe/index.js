const express = require('express');
const session = require('express-session');
const handlebars = require('express-handlebars');
const fs = require('fs');

const app = express();
const PORT = 3000;
const log = console.log;

// Middlwares

app.use(express.urlencoded({ extended: true }));
app.use(session({
  secret: 'test',
  resave: false,
  saveUninitialized: false
}));
app.set('views', __dirname);
app.engine('hbs', handlebars({
  defaultLayout: 'main',
  layoutsDir: __dirname,
  extname: '.hbs'
}));
app.set('view engine', 'hbs');

// Login

const login = (req, res, next) => {
  if (!req.session.userId) {
    res.redirect('/login');
  } else {
    next();
  }
};

// DB

const users = JSON.parse(fs.readFileSync('db.json'));

// Routers

app.get('/home', login, (req, res) => {
  res.send('Home page, must to be logged in to access it');
});

app.get('/login', (req, res) => {
  res.render('login');
});

app.post('/login', (req, res) => {
  if (!req.body.email || !req.body.password) {
    return res.status(400).send('Fill all the fields');
  }
  const user = users.find(user => user.email == req.body.email);

  if (!user && user.password !== req.body.password) {
    return res.status(400).send('Invalid credentials');
  }

  req.session.userId = user.id;
  // log(user);
  log(req.session);
  res.redirect('/home');
});

app.get('/edit', login, (req, res) => {
  res.render('/edit');
});

app.post('/edit', login, (req, res) => {
  const user = users.find(user => user.id == req.session.userId);

  user.email = req.body.email;
  log(`User ${user.id} email changed to ${user.email}`);
});

// Server

app.listen(PORT, () => {
  log('Listening on port', PORT);
});